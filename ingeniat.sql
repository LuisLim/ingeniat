create database ingeniat;

use ingeniat;

create table roles (
 role_id int auto_increment not null,
 role_name varchar(255) not null,
 role_description text not null,
 primary key (role_id)
 );
 
 insert into roles (role_name, role_description) 
 VALUES ("Básico", "Access"),
 ("Medio", "Check"),
 ("Medio Alto", "CR"),
 ("Alto Medio", "CRU"),
 ("Alto", "CRUD");
 
create table users (
user_id int auto_increment not null,
user_name varchar(255) not null,
user_lastname varchar(255) not null,
user_email varchar(255) unique not null,
user_password varchar(255) not null,
user_role_id int not null,
primary key (user_id),
foreign key (user_role_id) references roles(role_id)
 );
 
 create table posts (
 post_id int auto_increment not null,
 post_title varchar(255) not null,
 post_description text not null,
 post_date date not null,
 post_user_id int not null,
 primary key (post_id),
 foreign key (post_user_id) references users(user_id)
 );
 