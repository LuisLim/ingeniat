<?php
    chdir(dirname(__DIR__));

    use Firebase\JWT\JWT;
    use Firebase\JWT\Key;

    require_once('vendor/autoload.php');

    include('config.php');
try {
    if (! preg_match('/Bearer\s(\S+)/', $_SERVER['HTTP_AUTHORIZATION'], $matches)) {
        header('HTTP/1.0 400 Bad Request');
        echo 'Token not found in request';
        exit;
    }

    $jwt = $matches[1];
    if (! $jwt) {
        // No token was able to be extracted from the authorization header
        header('HTTP/1.0 400 Bad Request');
        exit;
    }
    $token = JWT::decode($jwt, new Key($secretKey, 'HS512'));
    $now = new DateTimeImmutable();
    $role;
    $user;

    if ($token->iss !== $serverName ||
        $token->nbf > $now->getTimestamp() ||
        $token->exp < $now->getTimestamp()) {
        header('HTTP/1.1 401 Unauthorized');
        exit;
    } else {
        $role = $token->roleID;
        $user = $token->userID;
    }

    header("Content-Type:application/json");
    $requestMethod = $_SERVER["REQUEST_METHOD"];
    } catch (Exception $e) {echo $e;}
    try {

            if (strtoupper($requestMethod) == 'GET' && $_GET['post_id']!="") {
        if ($role > 1) {
            if (isset($_GET['post_id']) && $_GET['post_id']!="") {
                include('db.php');
                $post_id = $_GET['post_id'];
                $result = mysqli_query(
                $con,
                "SELECT p.post_id, p.post_title, p.post_description, p.post_date, u.user_name, u.user_lastname, r.role_name".
                " FROM `posts` as p join `users` as u on p.post_user_id = u.user_id join `roles` as r on".
                " u.user_role_id = r.role_id WHERE post_id = $post_id");
                if (mysqli_num_rows($result) > 0) {
                    $row;
                    while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                        $row = array(
                            "post_id"=> $data[0],
                            "post_name"=> $data[1],
                            "post_description"=>  $data[2],
                            "post_date"=> $data[3],
                            "user_name"=> $data[4],
                            "user_lastname"=> $data[5],
                            "role_name"=> $data[6],
                        );
                    };
                    response($row, 200, "Success");
                    mysqli_close($con);
                } else{
                        response(NULL, 200, "No Record Found");
                }
            } else {
                response(NULL, 400, "Invalid Request");
            }
        } else {
            response(NULL, 403, "Not Authorized");
        }
    } else if (strtoupper($requestMethod) == 'GET') {
        if ($role > 1) {
            include('db.php');
            $result = mysqli_query(
            $con,
            "SELECT p.post_id, p.post_title, p.post_description, p.post_date, u.user_name, u.user_lastname, r.role_name".
                " FROM `posts` as p join `users` as u on p.post_user_id = u.user_id join `roles` as r on".
                " u.user_role_id = r.role_id");
            if(mysqli_num_rows($result) > 0) {
            $aResult = array();
            while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                $dataObj = array(
                    "post_id"=> $data[0],
                    "post_name"=> $data[1],
                    "post_description"=>  $data[2],
                    "post_date"=> $data[3],
                    "user_name"=> $data[4],
                    "user_lastname"=> $data[5],
                    "role_name"=> $data[6],
                );
                $aResult[] = $dataObj;
            };
            response($aResult, 200, "Success");
            mysqli_close($con);
            } else{
                    response(NULL, 200, "No Record Found");
            }
        } else {
            response(NULL, 403, "Not Authorized");
        }
    } else if (strtoupper($requestMethod) == 'DELETE') {
        if ($role > 4) {
            if (isset($_GET['post_id']) && $_GET['post_id']!="") {
                include('db.php');
                $post_id = $_GET['post_id'];
                if (mysqli_query(
                $con,
                "DELETE FROM `posts` WHERE post_id = $post_id")) {
                    response("Deleted", 200, "Success");
                    mysqli_close($con);
                } else{
                        response(NULL, 200, "No Record Found");
                }
            } else {
                response(NULL, 400, "Invalid Request");
            }
        } else {
            response(NULL, 403, "Not Authorized");
        }
    } else if (strtoupper($requestMethod) == 'POST') {
        if ($role > 2) {
            include('db.php');
            $rData = json_decode(file_get_contents("php://input"));
            $date = date("Y-m-d");
            $query = "INSERT INTO `posts` (post_title, post_description, post_date, post_user_id)".
            " VALUES('$rData->post_title', '$rData->post_description', '$date', '$user')";
            if (mysqli_query($con, $query) === TRUE) {
                response(mysqli_insert_id($con), 201, "Success");
                mysqli_close($con);
            } else {
                response(NULL, 200, "Error, check insert data");
            }
        } else {
            response(NULL, 403, "Not Authorized");
        }
    } else if (strtoupper($requestMethod) == 'PUT' || strtoupper($requestMethod) == 'PATCH') {
        if ($role > 3) {
            include('db.php');
            $post_id = $_GET['post_id'];
            $rData = json_decode(file_get_contents("php://input"));
            $date = date("Y-m-d");
            $query = "UPDATE `posts` SET post_title = '$rData->post_title', post_description = '$rData->post_description',".
            " post_user_id = $user WHERE post_id = $post_id";
            if (mysqli_query($con, $query) === TRUE) {
                response("Updated ".$post_id, 201, "Success");
                mysqli_close($con);
            } else {
                response(NULL, 200, "Error, check insert data");
            }
        } else {
            response(NULL, 401, "Not Authorized");
        }
    } else {
        response(NULL, 403, "Invalid Request");
    }

    } catch (Exception $e) {
        echo $e;
    }


    function response($row, $status, $message){
        $response['data'] = $row;
        $response['status'] = $status;
        $response['message'] = $message;

        $json_response = json_encode($response);
        echo $json_response;
    }
?>