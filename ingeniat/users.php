<?php
    declare(strict_types=1);

    use Firebase\JWT\JWT;

    require_once('./vendor/autoload.php');

    include('config.php');

    header("Content-Type:application/json");

    $requestMethod = $_SERVER["REQUEST_METHOD"];
    if (strtoupper($requestMethod) == 'GET') {
        try
        {
            include('db.php');
            $rData = json_decode(file_get_contents("php://input"));
            $result = mysqli_query(
            $con,
            "SELECT * FROM `users` WHERE user_email = '$rData->user_email' AND user_password = '$rData->user_password'");
            if (mysqli_num_rows($result) > 0) {
                $row;
                while ($data = mysqli_fetch_array($result, mysqli_num_rows($result))) {
                    $row = $data;
                };
                $data = [
                    'iat'  => $issuedAt->getTimestamp(),
                    'iss'  => $serverName,
                    'nbf'  => $issuedAt->getTimestamp(),
                    'exp'  => $expire,
                    'userID' => $row["user_id"],
                    'roleID' => $row["user_role_id"],
                ];
                $jwt = JWT::encode($data, $secretKey, 'HS512');
                $row["token"] = $jwt;
                response($row, 200, "Success");
                mysqli_close($con);
            } else{
                    response(NULL, 200, "No Record Found");
            }
        } catch (Exception $e) {
            echo $e;
        }

    } else if (strtoupper($requestMethod) == 'POST') {
        try {
            include('db.php');
            $rData = json_decode(file_get_contents("php://input"));
            $date = date("Y-m-d");
            $query = "INSERT INTO `users` (user_name, user_lastname, user_email, user_password, user_role_id)".
            " VALUES('$rData->user_name', '$rData->user_lastname', '$rData->user_email', '$rData->user_password', $rData->user_role_id)";
            if (mysqli_query($con, $query) === TRUE) {
                response(mysqli_insert_id($con), 201, "Success");
                mysqli_close($con);
            } else {
                response(NULL, 200, "Error, check insert data");
            }
        } catch (Exception $e) {
            echo $e;
        }

    } else {
        response(NULL, 404, "Wrong Request");
    }

    function response($row, $status, $message){
        $response['data'] = $row;
        $response['status'] = $status;
        $response['message'] = $message;

        $json_response = json_encode($response);
        echo $json_response;
    }
?>